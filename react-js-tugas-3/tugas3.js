// * Soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

// * Jawaban 1
function kapital(string) {
  return string[0].toUpperCase() + string.slice(1);
}

var jawabanSatu = kataPertama.concat(
  " ",
  kapital(kataKedua),
  " ",
  kataKetiga,
  " ",
  kataKeempat.toUpperCase()
);
console.log("Jawaban 1 : " + jawabanSatu);

// * Soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

// * Jawaban 2
var kataPertama = Number(kataPertama);
var kataKedua = Number(kataKedua);
var kataKetiga = Number(kataKetiga);
var kataKeempat = Number(kataKeempat);

var jawabanDua = kataPertama + kataKedua + kataKetiga + kataKeempat;
console.log("Jawaban 2 : " + jawabanDua);

// * Soal 3
var kalimat = "wah javascript itu keren sekali";

var kataPertama = kalimat.substring(0, 3);
// * Jawaban 3
var kataKedua = kalimat.substring(3, 15);
var kataKetiga = kalimat.substring(15, 19);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(24);

console.log("Jawaban 3 : ");
console.log("Kata Pertama: " + kataPertama);
console.log("Kata Kedua: " + kataKedua);
console.log("Kata Ketiga: " + kataKetiga);
console.log("Kata Keempat: " + kataKeempat);
console.log("Kata Kelima: " + kataKelima);

// * Soal 4
var nilai = 75;

// * Jawaban 4
console.log("Jawaban 4 : ");
var nilai = Number(nilai);
if (nilai >= 80) {
  console.log("A");
} else if (70 <= nilai && nilai < 80) {
  console.log("B");
} else if (60 <= nilai && nilai < 70) {
  console.log("C");
} else if (50 <= nilai && nilai < 60) {
  console.log("D");
} else console.log("E");

// * Soal 5
var tanggal = 06;
var bulan = 11;
var tahun = 1995;

// * Jawaban 5
console.log("Jawaban 5 : ");
switch (bulan) {
  case 1:
    bulan = "Januari";
    console.log(bulan);
    break;
  case 2:
    bulan = "Februari";
    console.log(bulan);
    break;
  case 3:
    bulan = "Maret";
    console.log(bulan);
    break;
  case 4:
    bulan = "April";
    console.log(bulan);
    break;
  case 5:
    bulan = "Mei";
    console.log(bulan);
    break;
  case 6:
    bulan = "Juni";
    console.log(bulan);
    break;
  case 7:
    bulan = "Juli";
    console.log(bulan);
    break;
  case 8:
    bulan = "Agustus";
    console.log(bulan);
    break;
  case 9:
    bulan = "September";
    console.log(bulan);
    break;
  case 10:
    bulan = "Oktober";
    console.log(bulan);
    break;
  case 11:
    bulan = "Nopember";
    console.log(bulan);
    break;
  case 12:
    bulan = "Desember";
    console.log(bulan);
    break;
}

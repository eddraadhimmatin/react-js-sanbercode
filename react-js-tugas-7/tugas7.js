// * Soal 1 Release 0
class Animal {
  constructor(hewan) {
    this._name = hewan;
    this._legs = 4;
    this._cold_blooded = false;
  }
  get name() {
    return this._name;
  }
  get legs() {
    return this._legs;
  }
  get cold_blooded() {
    return this._cold_blooded;
  }
}

var sheep = new Animal("shaun");
console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded); // false

// * Soal 1 Release 1
class Ape extends Animal {
  constructor(hewan, kaki) {
    super(hewan);
    this._legs = kaki;
  }
  yell() {
    console.log("Auooo");
  }
}

class Frog extends Animal {
  constructor(hewan, darah) {
    super(hewan);
    this._cold_blooded = darah;
  }
  jump() {
    console.log("hop hop");
  }
}

var sungokong = new Ape("kera sakti", 2);
console.log(sungokong.name); // kera sakti
console.log(sungokong.legs); // 2
console.log(sungokong.cold_blooded); // false
sungokong.yell(); // "Auooo"

var kodok = new Frog("buduk", true);
console.log(kodok.name); // kera sakti
console.log(kodok.legs); // 2
console.log(kodok.cold_blooded); // true
kodok.jump(); // "hop hop"

// * Soal 2
class Clock {
  constructor({ template }) {
    this._template = template;
  }
  render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = "0" + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = "0" + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = "0" + secs;

    var output = this._template
      .replace("h", hours)
      .replace("m", mins)
      .replace("s", secs);

    console.log(output);
  }
  stop() {
    clearInterval(timer);
  }
  start() {
    this.render();
    this._timer = setInterval(this.render.bind(this), 1000);
  }
}
var clock = new Clock({ template: "h:m:s" });
clock.start();

// * Soal 1
console.log("Soal 1");
let luasLingkaran = (r) => r * r * Math.PI;
console.log(luasLingkaran(10));

let kelLingkaran = (r) => (r + r) * Math.PI;
console.log(kelLingkaran(10));

// * Soal 2
console.log("\nSoal 2");
let tambahKata = (kata1, kata2, kata3, kata4, kata5) => {
  let kalimat = `${kata1}${kata2}${kata3}${kata4}${kata5}`;
  console.log(kalimat);
};
tambahKata("saya ", "adalah ", "seorang ", "frontend ", "developer ");

// * Soal 3
console.log("\nSoal 3");
const newFunction = function literal(firstName, lastName) {
  return {
    firstName,
    lastName,
    fullName() {
      console.log(firstName + " " + lastName);
    },
  };
};
//Driver Code
newFunction("William", "Imoh").fullName();

// * Soal 4
console.log("\nSoal 4");
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!",
};

const { firstName, lastName, destination, occupation, spell } = newObject;
console.log(firstName, lastName, destination, occupation, spell);

// * Soal 5
console.log("\nSoal 5");
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
console.log(combined);

// * Soal 1
console.log("Soal 1");
function halo() {
  return "Halo Sanbers!";
}

console.log(halo());

// * Soal 2
console.log("\nSoal 2");
function kalikan(num1, num2) {
  return num1 * num2;
}
var num1 = 12;
var num2 = 4;
var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

// * Soal 3
console.log("\nSoal 3");
function introduce(name, age, address, hobby) {
  return (
    "Nama saya " +
    name +
    ", umur saya " +
    age +
    " tahun, alamat saya di " +
    address +
    ", dan saya punya hobby yaitu " +
    hobby
  );
}
var name = "John";
var age = 30;
var address = "Jalan belum jadi";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);

// * Soal 4
console.log("\nSoal 4");
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992];
var daftarPeserta = {
  nama: arrayDaftarPeserta[0],
  jenisKelamin: arrayDaftarPeserta[1],
  hobi: arrayDaftarPeserta[2],
  tahunLahir: arrayDaftarPeserta[3],
};
console.debug(daftarPeserta);

// * Soal 5
console.log("\nSoal 5");
var buah = [
  { nama: "strawberry", warna: "merah", adaBijinya: "tidak", harga: "9000" },
  { nama: "jeruk", warna: "oranye", adaBijinya: "ada", harga: "8000" },
  {
    nama: "Semangka",
    warna: "Hijau & Merah",
    adaBijinya: "ada",
    harga: "10000",
  },
  { nama: "Pisang", warna: "Kuning", adaBijinya: "tidak", harga: "5000" },
];

var arrayBuahFilter = buah.filter(function (item) {
  return item.nama == "strawberry" || item.warna == "merah";
});

console.debug(arrayBuahFilter);

// * Soal 6
console.log("\nSoal 6");
var dataFilm = [];

function tambah(item) {
  return item.concat("nama", "durasi", "genre", "tahun");
}

var hasilTambah = tambah(dataFilm);
console.log(hasilTambah);

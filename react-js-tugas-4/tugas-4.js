// * Soal 1
console.log("Soal 1");
console.log("LOOPING PERTAMA");
var i = 0;
while (i != 20) {
  i = i + 2;
  console.log(i + " - I love coding");
}

console.log("LOOPING KEDUA");
var i = 22;
while (i != 0) {
  i = i - 2;
  console.log(i + " - I will become frontend developer");
}

// * Soal 2
console.log("\n Soal 2");
for (var i = 1; i <= 20; i++) {
  if (i % 3 == 0 && i % 2 !== 0) {
    console.log(i + " I Love Coding");
  } else if (Math.abs(i % 2) == 1) {
    console.log(i + " Santai");
  } else if (i % 2 == 0) {
    console.log(i + " Berkualitas");
  }
}

// * Soal 3
console.log("\n Soal 3");
var pager = "#";
while (pager.length <= 7) {
  console.log(pager);
  pager = pager + "#"; // <-- this is the last statement executed, so it is returned
}

// * Soal 4
console.log("\n Soal 4");
var kalimat = "saya sangat senang belajar javascript";
var potong = kalimat.split(" ");
console.log(potong);

// * Soal 5
console.log("\n Soal 5");
var daftarBuah = [
  "2. Apel",
  "5. Jeruk",
  "3. Anggur",
  "4. Semangka",
  "1. Mangga",
];
daftarBuah.sort();
console.log(daftarBuah);

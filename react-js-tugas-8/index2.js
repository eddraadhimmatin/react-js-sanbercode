var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

var time = 10000;
readBooksPromise(time, books[0])
  .then(function (baca) {
    time = baca;
    console.log(baca);
    readBooksPromise(time, books[1])
      .then(function (baca) {
        time = baca;
        console.log(baca);
        readBooksPromise(time, books[2])
          .then(function (baca) {
            time = baca;
            console.log(baca);
          })
          .catch(function (berhenti) {
            console.log(berhenti);
          });
      })
      .catch(function (berhenti) {
        console.log(berhenti);
      });
  })
  .catch(function (berhenti) {
    console.log(berhenti);
  });

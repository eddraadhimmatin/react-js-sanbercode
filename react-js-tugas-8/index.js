var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
  { name: "komik", timeSpent: 1000 },
];

var time = 10000;
readBooks(time, books[0], function (sisa) {
  time = sisa;
  readBooks(time, books[1], function (sisa) {
    time = sisa;
    readBooks(time, books[2], function (sisa) {
      time = sisa;
      readBooks(time, books[3], function (sisa) {
        time = sisa;
      });
    });
  });
});
